import pytest
import app_shop.models
from django.core import management


@pytest.mark.django_db
def test_load_products():
    management.call_command('load_test_data')
    assert len(app_shop.models.Product.objects.all()) == 877
    assert len(app_shop.models.VendorCommissions.objects.all()) == 540
    assert len(app_shop.models.Promotion.objects.all()) == 5
    assert len(app_shop.models.ProductPromotion.objects.all()) == 600
    assert len(app_shop.models.Order.objects.all()) == 438
    assert len(app_shop.models.OrderLine.objects.all()) == 5539

# TODO: Hypothetical todo, to add tests checking the database sample
