from django.core import management
from django.test import Client
import pytest


# TODO: Hypothetically, break the large test data sets into a much smaller more manageable set inside the python files
#       themselves and test on these for the vast majority of the tests.
#       Only test on the full data set for a few of the tests.

# TODO: Include permissions.


@pytest.mark.django_db()
def test_get_daily_info():
    management.call_command('load_test_data')
    client = Client()
    url = '/daily_figures/2019-08-01'
    response = client.get(url)
    assert response.status_code == 200
    assert response.json() == {'customers': 9,
                               'discount_rate_avg': pytest.approx(0.1252497888),
                               'items': 2895,
                               'order_total_avg': pytest.approx(1182286.09604637),
                               'total_discount_amount': pytest.approx(130429980.2630924),
                               'commissions': {'total': pytest.approx(1416982214.355974),
                                               'promotions':  {'1': pytest.approx(171112103.7005636),
                                                               '2': pytest.approx(183069895.8455145),
                                                               '3': pytest.approx(205589233.822453),
                                                               '4': pytest.approx(216968130.3225221),
                                                               '5': pytest.approx(202910337.3433225)},
                                               'order_average': pytest.approx(255819.13962014)}}


@pytest.mark.django_db()
def test_response_if_no_data_for_date():
    client = Client()
    url = '/daily_figures/2122-01-02'
    response = client.get(url)
    assert response.status_code == 200
    assert response.json() == {'customers': 0,
                               'discount_rate_avg': None,
                               'items': None,
                               'order_total_avg': None,
                               'total_discount_amount': None,
                               'commissions': {'order_average': None, 'promotions': {}, 'total': None}
                               }
