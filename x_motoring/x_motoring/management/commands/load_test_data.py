import os
import datetime
import pandas as pd
from django.db import transaction
from app_shop import models
from django.core.management.base import BaseCommand


_FILE_DIR = os.path.dirname(os.path.abspath(__file__))
_COMMAND_DIR = os.path.join(os.path.dirname(_FILE_DIR), 'command_data')


class Command(BaseCommand):

    def handle(self, *args, **options):
        self._load_vendor_commissions()
        self._load_product()
        self._load_promotion()
        self._load_product_promotion()
        self._load_orders()
        self._load_order_lines()

    def _load_vendor_commissions(self):
        df = pd.read_csv(os.path.join(_COMMAND_DIR, 'commissions.csv'))

        objects = []
        for date, vendor_id, rate in df[['date', 'vendor_id', 'rate']].values:
            objects.append(models.VendorCommissions(date=datetime.datetime.strptime(date, '%Y-%m-%d'),
                                                    vendor_id=int(vendor_id),
                                                    rate=rate))
        models.VendorCommissions.objects.bulk_create(objects)

    def _load_product(self):
        df = pd.read_csv(os.path.join(_COMMAND_DIR, 'products.csv'))
        with transaction.atomic(savepoint=False):
            for count, (id_, description) in enumerate(df[['id', 'description']].values):
                #print(count, (id_, description))
                assert count + 1 == id_
                models.Product(description=description).save()

        objs = list(models.Product.objects.all())
        for i, description in enumerate(df['description'].values):
            assert objs[i].description == description

    def _load_promotion(self):
        df = pd.read_csv(os.path.join(_COMMAND_DIR, 'promotions.csv'))
        assert len(set(df['description'].values)) == len(df['description'].values)
        with transaction.atomic(savepoint=False):
            for description in df['description'].values:
                models.Promotion.objects.update_or_create(description=description)

    def _load_product_promotion(self):
        df = pd.read_csv(os.path.join(_COMMAND_DIR, 'product_promotions.csv'))
        with transaction.atomic(savepoint=False):
            for date, product_id, promotion_id in df[['date', 'product_id', 'promotion_id']].values:
                product = models.Product.objects.get(id=product_id)
                promotion = models.Promotion.objects.get(id=promotion_id)
                models.ProductPromotion.objects.update_or_create(date=datetime.datetime.strptime(date, '%Y-%m-%d'),
                                                                 product_id=product,
                                                                 promotion_id=promotion)
    def _load_orders(self):
        df = pd.read_csv(os.path.join(_COMMAND_DIR, 'orders.csv'))
        with transaction.atomic(savepoint=False):
            for id_, created_at, vendor_id, customer_id in df[['id', 'created_at', 'vendor_id', 'customer_id']].values:
                models.Order.objects.create(
                    id=id_,
                    created_at=datetime.datetime.strptime(created_at, '%Y-%m-%d %H:%M:%S.%f'),
                    vendor_id=vendor_id,
                    customer_id=customer_id)

    def _load_order_lines(self):
        df = pd.read_csv(os.path.join(_COMMAND_DIR, 'order_lines.csv'))
        with transaction.atomic(savepoint=False):
            for i, row in df.iterrows():
                models.OrderLine.objects.create(
                    order_id=models.Order.objects.get(id=row['order_id']),
                    product_id=models.Product.objects.get(id=row['product_id']),
                    product_description=row['product_description'],
                    product_price=row['product_price'],
                    product_vat_rate=row['product_vat_rate'],
                    discount_rate=row['discount_rate'],
                    quantity=row['quantity'],
                    full_price_amount=row['full_price_amount'],
                    discounted_amount=row['discounted_amount'],
                    vat_amount=row['vat_amount'],
                    total_amount=row['total_amount'])
