import datetime
from django.db import models, connection


class SafeModel(models.Model):

    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        # respect the null = False
        self.full_clean()
        super().save(*args, **kwargs)


class DailyManager(models.Manager):

    def daily(self, date_to_search_from: datetime.datetime):
        date_to_search_end = date_to_search_from + datetime.timedelta(days=1)
        return self.filter(created_at__gte=date_to_search_from).filter(created_at__lt=date_to_search_end)


class Order(SafeModel):

    objects = DailyManager()
    created_at = models.DateTimeField()
    vendor_id = models.PositiveIntegerField()
    customer_id = models.PositiveIntegerField()

    @classmethod
    def get_number_customers_who_ordered(cls, date: datetime.datetime):
        return len(set([model.customer_id for model in cls.objects.daily(date).all()]))


class OrderLine(SafeModel):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    product_id = models.ForeignKey('Product', on_delete=models.CASCADE)
    product_description = models.TextField(max_length=1000)
    product_price = models.FloatField()
    product_vat_rate = models.FloatField()
    discount_rate = models.FloatField()
    quantity = models.PositiveIntegerField()
    full_price_amount = models.FloatField()
    discounted_amount = models.FloatField()
    vat_amount = models.FloatField()
    total_amount = models.FloatField()

    @classmethod
    def get_daily_orderline(cls, date: datetime.datetime):
        orders = Order.objects.daily(date)
        return cls.objects.filter(order_id__in=orders)


class Product(SafeModel):
    description = models.CharField(max_length=200)   # Probably should be unique but the data is not.


class Promotion(SafeModel):
    description = models.TextField(max_length=1000, unique=True)


class ProductPromotion(SafeModel):
    date = models.DateTimeField()
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    promotion_id = models.ForeignKey(Promotion, on_delete=models.CASCADE)


class VendorCommissions(SafeModel):
    date = models.DateTimeField()
    vendor_id = models.PositiveIntegerField()
    rate = models.FloatField()

    @classmethod
    def get_total_commissions(cls, date: datetime.datetime) -> float:
        # TODO: as vendor_id is not a foreign key cannot use django ORM selected related
        #       therefore querying using raw SQL.
        with connection.cursor() as cursor:
            query = """
            SELECT SUM(orderline.total_amount * commissions.rate) FROM `app_shop_vendorcommissions` as commissions
             INNER JOIN `app_shop_order` as orders
             ON commissions.vendor_id = orders.vendor_id
             INNER JOIN `app_shop_orderline` as orderline
             ON orders.id = orderline.order_id_id
             WHERE (commissions.date >= %s AND commissions.date < %s);
            """
            cursor.execute(query, [date, date + datetime.timedelta(days=1)])
            row = cursor.fetchone()
        return row[0]

    @classmethod
    def get_average_commission_per_order(cls, date: datetime.datetime) -> float:
        # TODO: as vendor_id is not a foreign key cannot use django ORM selected related
        #       therefore querying using raw SQL.
        with connection.cursor() as cursor:
            query = """
            SELECT AVG(orderline.total_amount * commissions.rate) FROM `app_shop_vendorcommissions` as commissions
             INNER JOIN `app_shop_order` as orders
             ON commissions.vendor_id = orders.vendor_id
             INNER JOIN `app_shop_orderline` as orderline
             ON orders.id = orderline.order_id_id
             WHERE (commissions.date >= %s AND commissions.date < %s);
            """
            cursor.execute(query, [date, date + datetime.timedelta(days=1)])
            row = cursor.fetchone()
        return row[0]

    @classmethod
    def get_total_commission_per_promotion(cls, date: datetime.datetime) -> dict:
        query = """
        SELECT promotion.promotion_id_id, SUM(orderline.total_amount * commissions.rate)
         FROM `app_shop_vendorcommissions` as commissions
         INNER JOIN `app_shop_order` as orders
         ON commissions.vendor_id = orders.vendor_id
         INNER JOIN `app_shop_orderline` as orderline
         ON orders.id = orderline.order_id_id
         INNER JOIN `app_shop_productpromotion` as promotion
         ON orderline.product_id_id = promotion.product_id_id
         WHERE (commissions.date >= %s AND commissions.date < %s)
         GROUP BY promotion.promotion_id_id;
        """
        with connection.cursor() as cursor:
            cursor.execute(query, [date, date + datetime.timedelta(days=1)])
            result = cursor.fetchall()
        return dict(result)
