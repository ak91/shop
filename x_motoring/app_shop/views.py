import datetime
import app_shop.models
from django.db.models import Sum, Avg
from django.http import JsonResponse


def get_daily_figures(request, date: str):
    date = datetime.datetime.strptime(date, '%Y-%m-%d')
    daily_orderline = app_shop.models.OrderLine.get_daily_orderline(date)

    data = {
        'customers': app_shop.models.Order.get_number_customers_who_ordered(date),
        'total_discount_amount': daily_orderline.aggregate(Sum('discounted_amount'))['discounted_amount__sum'],
        'items': daily_orderline.aggregate(Sum('quantity'))['quantity__sum'],
        'order_total_avg': daily_orderline.aggregate(Avg('total_amount'))['total_amount__avg'],
        'discount_rate_avg': daily_orderline.aggregate(Avg('discount_rate'))['discount_rate__avg'],
        'commissions': {'total': app_shop.models.VendorCommissions.get_total_commissions(date),
                        'order_average':  app_shop.models.VendorCommissions.get_average_commission_per_order(date),
                        'promotions': app_shop.models.VendorCommissions.get_total_commission_per_promotion(date)}
    }
    return JsonResponse(data)
