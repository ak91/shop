from django.urls import path
from app_shop import views

urlpatterns = [
    path("daily_figures/<str:date>", views.get_daily_figures),
]