# README #

Example of an online shop application.

### How do I get set up? ###

Python Version: 3.8.9

To install requirements:

* pip install -r requirements

To run the tests cd to the x_motoring directory and run the following commands:

* python manage.py makemigrations
* python manage.py migrate
* pytest test

This will run the tests in the default sqlite database.

In order to load the test data into this database run the following management command:

* python manage.py load_test_data
